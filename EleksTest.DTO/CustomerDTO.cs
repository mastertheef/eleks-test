﻿using System.Collections.Generic;

namespace EleksTest.DTO
{
    public class CustomerDto
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string MobileNo { get; set; }
        public List<TransactionDto> Transactions { get; set; }
    }
}
