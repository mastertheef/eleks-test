﻿using EleksTest.ObjectModel.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace EleksTest.DTO
{
    public class TransactionDto
    {
        public long Id { get; set; }
        public long CustomerId { get; set; }
        public string Date { get; set; }
        public decimal Amount { get; set; }
        public string Code { get; set; }
        public string Status { get; set; }
    }
}
