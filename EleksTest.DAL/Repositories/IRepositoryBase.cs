﻿using EleksTest.ObjectModel;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace EleksTest.DAL.Repositories
{
    public interface IRepositoryBase<T> where T: class, IEntity, new()
    {
        Task<T> GetByIdAsync(long id);
        Task<List<T>> GetAllAsync();
        Task<T> SaveOrUpdateAsync(T entity);
    }
}
