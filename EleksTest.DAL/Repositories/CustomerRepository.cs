﻿using EleksTest.ObjectModel;
using Microsoft.EntityFrameworkCore;
using System.Threading.Tasks;

namespace EleksTest.DAL.Repositories
{
    public class CustomerRepository : RepositoryBase<Customer>, ICustomerRepository
    {
        public CustomerRepository(EleksDbContext dbContext) : base(dbContext)
        {
        }

        public async Task<Customer> GetCustomer(long? id, string email)
        {
            return await _dbSet.Include(x=>x.Transactions).FirstOrDefaultAsync(x =>
                 (id.HasValue ? x.Id == id.Value : true) &&
                 (!string.IsNullOrEmpty(email) ? x.Email == email : true));
        }
    }
}
