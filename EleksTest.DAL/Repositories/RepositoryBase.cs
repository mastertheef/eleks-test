﻿using EleksTest.ObjectModel;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace EleksTest.DAL.Repositories
{
    public class RepositoryBase<T> : IRepositoryBase<T> where T : class, IEntity, new()
    {
        protected readonly EleksDbContext _dbContext;
        protected readonly DbSet<T> _dbSet;

        public RepositoryBase(EleksDbContext dbContext)
        {
            _dbContext = dbContext;
            _dbSet = _dbContext.Set<T>();
        }

        public async Task<List<T>> GetAllAsync() => await _dbSet.ToListAsync();

        public async Task<T> GetByIdAsync(long id) => await _dbSet.FirstOrDefaultAsync(x => x.Id == id);

        public async Task<T> SaveOrUpdateAsync(T entity)
        {
            var found = _dbSet.AsNoTracking().FirstOrDefaultAsync(x => x.Id == entity.Id);
            if (found == null)
            {
                _dbSet.Attach(entity);
            }

            await _dbContext.SaveChangesAsync();
            return entity;
        }
    }
}
