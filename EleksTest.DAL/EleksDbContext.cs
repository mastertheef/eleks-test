﻿using EleksTest.ObjectModel;
using EleksTest.ObjectModel.Enums;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Storage;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Threading.Tasks;

namespace EleksTest.DAL
{
    public class EleksDbContext : DbContext
    {
        public DbSet<Transaction> Transactions { get; set; }
        public DbSet<Customer> Customers { get; set; }

        public EleksDbContext(DbContextOptions options) : base(options)
        {
            
        }

        protected EleksDbContext()
        {
            Database.EnsureCreated();
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Entity<Customer>()
                .HasMany(x => x.Transactions)
                .WithOne(x => x.Customer)
                .HasForeignKey(x => x.CustomerId)
                .OnDelete(DeleteBehavior.Cascade);

            modelBuilder.Entity<Customer>()
                .Property(x => x.Id)
                .ValueGeneratedNever();

            modelBuilder.Entity<Transaction>()
                .Property(x => x.Code)
                .HasConversion<string>();

            modelBuilder.Entity<Transaction>()
                .Property(x => x.Status)
                .HasConversion<string>();

            modelBuilder.Entity<Transaction>()
                .Property(x => x.Id)
                .ValueGeneratedNever();
        }

        public async Task SeedData()
        {
            bool customersSeeded = await Customers.AnyAsync();
            bool transactionsSeeded = await Transactions.AnyAsync();

            if (!customersSeeded)
            {
                Customers.AddRange(GetInitialCustomers());
            }

            if (!transactionsSeeded)
            {
                Transactions.AddRange(GetInitialTransactions());
            }
            SaveChanges();
        }

        private List<Customer> GetInitialCustomers()
        {
            return new List<Customer>
            {
                new Customer
                {
                    Id = 1,
                    Email = "john_snow@email.com",
                    MobileNo = "0501111111",
                    Name = "Jonh Snow"
                },
                new Customer
                {
                    Id = 2,
                    Email = "sansa_stark@email.com",
                    MobileNo = "0502222222",
                    Name = "Sansa Stark"
                },
                new Customer
                {
                    Id = 3,
                    Email = "tywin_lannister@email.com",
                    MobileNo = "0503333333",
                    Name = "Tywin Lannister"
                }
            };
        }

        private List<Transaction> GetInitialTransactions()
        {
            return new List<Transaction>
            {
                new Transaction
                {
                    Id = 1,
                    CustomerId = 2,
                    Amount = 100.00m,
                    Date = DateTime.UtcNow,
                    Code = CurrencyCode.JPY,
                    Status = TransactionStatus.Failed
                },
                new Transaction
                {
                    Id = 2,
                    CustomerId = 3,
                    Amount = 1000.45m,
                    Date = DateTime.UtcNow,
                    Code = CurrencyCode.USD,
                    Status = TransactionStatus.Success
                },
                new Transaction
                {
                    Id = 3,
                    CustomerId = 3,
                    Amount = 12.88m,
                    Date = DateTime.UtcNow,
                    Code = CurrencyCode.THB,
                    Status = TransactionStatus.Canceled
                },
                new Transaction
                {
                    Id = 4,
                    CustomerId = 3,
                    Amount = 999.00m,
                    Date = DateTime.UtcNow,
                    Code = CurrencyCode.SGD,
                    Status = TransactionStatus.Success
                }
            };
        }
    }
}
