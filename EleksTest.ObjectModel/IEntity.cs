﻿namespace EleksTest.ObjectModel
{
    public interface IEntity
    {
        long Id { get; set; }
    }
}
