﻿namespace EleksTest.ObjectModel.Enums
{
    public enum TransactionStatus
    {
        Success,
        Failed,
        Canceled
    }
}
