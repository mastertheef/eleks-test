﻿namespace EleksTest.ObjectModel.Enums
{
    public enum CurrencyCode
    {
        USD,
        JPY,
        THB,
        SGD
    }
}
