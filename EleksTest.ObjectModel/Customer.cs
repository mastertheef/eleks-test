﻿using System.Collections.Generic;

namespace EleksTest.ObjectModel
{
    public class Customer : IEntity
    {
        public long Id { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string MobileNo { get; set; }
        public virtual ICollection<Transaction> Transactions { get; set; }
    }
}
