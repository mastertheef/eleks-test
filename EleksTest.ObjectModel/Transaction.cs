﻿using EleksTest.ObjectModel.Enums;
using System;

namespace EleksTest.ObjectModel
{
    public class Transaction : IEntity
    {
        public long Id { get; set; }
        public long CustomerId { get; set; }
        public DateTime Date { get; set; }
        public decimal Amount { get; set; }
        public CurrencyCode Code { get; set; }
        public TransactionStatus Status { get; set; }
        public virtual Customer Customer { get; set; }
    }
}
