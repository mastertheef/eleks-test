# Test task repository

### Configuration
The only thing in solution to be configured is the **"DefaultConnection"** connection string in **appconfig.json** file located in** EleksTest.API** project. It should lead to working SQL server. No need to create database. The application will create it and seed initial data on the first call to api endpoint.

### API
- api/customers?id={id}&email={email}

### Sample requests
- api/customers?id=1
- api/customers?email=sansa_stark@email.com
- api/customers?id=3&email=tywin_lannister@email.com

### Features
- There is implemented logic to continue development of API
- POST, PUT, DELETE methods for Customer and Transaction entities from this point can be implemented fast and easy

### TODO
- Implement end to end tests
- Implement logging
- Integrate Swagger or Application Insights

