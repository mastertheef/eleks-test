﻿using EleksTest.ObjectModel;
using System.Threading.Tasks;

namespace EleksTest.Services
{
    public interface ICustomerService : IServiceBase<Customer>
    {
        Task<Customer> GetCustomer(long? id, string email);
    }
}
