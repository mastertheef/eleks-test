﻿using EleksTest.ObjectModel;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace EleksTest.Services
{
    public interface IServiceBase<T> where T : class, IEntity, new()
    {
        Task<T> GetByIdAsync(long id);
        Task<List<T>> GetAllAsync();
        Task<T> SaveOrUpdateAsync(T entity);
    }
}
