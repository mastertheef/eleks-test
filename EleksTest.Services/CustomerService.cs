﻿using EleksTest.DAL.Repositories;
using EleksTest.ObjectModel;
using System.Threading.Tasks;

namespace EleksTest.Services
{
    public class CustomerService : ServiceBase<Customer>, ICustomerService
    {
        private ICustomerRepository _repository;

        public CustomerService(ICustomerRepository repository) : base(repository)
        {
            _repository = repository;
        }

        public async Task<Customer> GetCustomer(long? id, string email) => await _repository.GetCustomer(id, email);
    }
}
