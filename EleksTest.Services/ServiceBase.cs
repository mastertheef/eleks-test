﻿using EleksTest.DAL.Repositories;
using EleksTest.ObjectModel;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace EleksTest.Services
{
    public class ServiceBase<T> : IServiceBase<T> where T : class, IEntity, new()
    {
        private readonly IRepositoryBase<T> _repository;

        public ServiceBase(IRepositoryBase<T> repository)
        {
            _repository = repository;
        }

        public async Task<List<T>> GetAllAsync() => await _repository.GetAllAsync();

        public async Task<T> GetByIdAsync(long id) => await _repository.GetByIdAsync(id);

        public async Task<T> SaveOrUpdateAsync(T entity) => await _repository.SaveOrUpdateAsync(entity);
    }
}
