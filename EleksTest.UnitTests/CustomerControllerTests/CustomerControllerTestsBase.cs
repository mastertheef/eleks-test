﻿using AutoMapper;
using EleksTest.API.Controllers;
using EleksTest.Services;
using Moq;

namespace EleksTest.UnitTests.CustomerControllerTests
{
    public class CustomerControllerTestsBase
    {
        protected Mock<ICustomerService> _customerServiceMock;
        protected Mock<IMapper> _mapperMock;

        public CustomerControllerTestsBase()
        {
            _customerServiceMock = new Mock<ICustomerService>();
            _mapperMock = new Mock<IMapper>();
        }

        public CustomersController MakeController()
        {
            return new CustomersController(_customerServiceMock.Object, _mapperMock.Object);
        } 
    }
}
