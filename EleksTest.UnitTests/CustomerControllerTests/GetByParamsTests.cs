﻿using EleksTest.API.Models;
using EleksTest.DTO;
using EleksTest.ObjectModel;
using Microsoft.AspNetCore.Mvc;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace EleksTest.UnitTests.CustomerControllerTests
{
    [TestFixture]
    public class GetByParamsTests : CustomerControllerTestsBase
    {
        [Test]
        public async Task ItShouldReturnCustomerIfIdIsProvided()
        {
            const long id = 1;
            var model = new GetCustomerModel { id = id };
            var customer = new Customer { Id = 1 };
            var mapped = new CustomerDto { Id = 1 };

            _customerServiceMock.Setup(x => x.GetCustomer(id, null)).ReturnsAsync(customer);
            _mapperMock.Setup(x => x.Map<CustomerDto>(customer)).Returns(mapped);

            var controller = MakeController();

            var result = await controller.Get(model);

            Assert.AreEqual(mapped, result.Value);
        }

        [Test]
        public async Task ItShouldReturnCustomerIfEmailIsProvided()
        {
            const string email = "some@email.com";
            var model = new GetCustomerModel { email = email };
            var customer = new Customer { Id = 1 };
            var mapped = new CustomerDto { Id = 1 };

            _customerServiceMock.Setup(x => x.GetCustomer(null, email)).ReturnsAsync(customer);
            _mapperMock.Setup(x => x.Map<CustomerDto>(customer)).Returns(mapped);

            var controller = MakeController();

            var result = await controller.Get(model);

            Assert.AreEqual(mapped, result.Value);
        }

        [Test]
        public async Task ItShouldReturnNotFoundIfNothingFoundByProvidedParams()
        {
            var model = new GetCustomerModel
            {
                id = 1,
                email = "some@email.com"
            };

            var controller = MakeController();

            var result = await controller.Get(model);

            Assert.IsInstanceOf<NotFoundResult>(result.Result);
        }

        [Test]
        public async Task ItShouldReturnBadRequestIfNoQueryParametersAreProvided()
        {
            var model = new GetCustomerModel();
            var controller = MakeController();

            var result = await controller.Get(model);

            Assert.IsInstanceOf<BadRequestObjectResult>(result.Result);
        }

        [Test]
        public async Task ItShouldReturnBadRequestIfIdIncorrect()
        {
            const long id = 99999999999;
            var model = new GetCustomerModel { id = id };
            var controller = MakeController();
            controller.ModelState.AddModelError("id", "Invalid ID");
            var result = await controller.Get(model);

            Assert.IsInstanceOf<BadRequestObjectResult>(result.Result);
        }

        [Test]
        public async Task ItShouldReturnBadRequestEmailIsIncorrect()
        {
            string email = "some incorrect email";
            var model = new GetCustomerModel { email = email };
            var controller = MakeController();
            controller.ModelState.AddModelError("email", "Invalid Email");
            var result = await controller.Get(model);

            Assert.IsInstanceOf<BadRequestObjectResult>(result.Result);
        }
    }
}
