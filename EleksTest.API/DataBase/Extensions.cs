﻿using EleksTest.DAL;
using Microsoft.AspNetCore.Builder;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

namespace EleksTest.API.DataBase
{
    public static class Extensions
    {
        public static void CreateDataBaseAndSeed(this IApplicationBuilder app)
        {
            using (var serviceScope = app.ApplicationServices.GetService<IServiceScopeFactory>().CreateScope())
            {
                var context = serviceScope.ServiceProvider.GetRequiredService<EleksDbContext>();
                context.Database.Migrate();
                context.SeedData().GetAwaiter().GetResult();
            }
        }
    }
}
