﻿using AutoMapper;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;

namespace EleksTest.API.AutoMapper
{
    public static class Extensions
    {
        public static void AddAutomapper(this IServiceCollection services)
        {
            services.AddSingleton(c =>
            {
                var appConfig = c.GetService<IOptions<AppConfig>>();
                var mappingConfig = new MapperConfiguration(mc =>
                {
                    mc.AddProfile(new MappingProfile(appConfig.Value));
                });
                return mappingConfig.CreateMapper();
            });
        }
    }
}
