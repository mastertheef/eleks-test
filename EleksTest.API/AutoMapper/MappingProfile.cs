﻿using AutoMapper;
using EleksTest.DTO;
using EleksTest.ObjectModel;

namespace EleksTest.API.AutoMapper
{
    public class MappingProfile : Profile
    {
        private readonly AppConfig _appConfig;

        public MappingProfile(AppConfig appConfig)
        {
            _appConfig = appConfig;

            CreateMap<Customer, CustomerDto>();
            CreateMap<Transaction, TransactionDto>()
                .ForMember(x=>x.Date, opt => opt.MapFrom(x=>x.Date.ToString(_appConfig.DateFormat)));
        }
    }
}
