﻿using System.ComponentModel.DataAnnotations;

namespace EleksTest.API.Models
{
    public class GetCustomerModel
    {
        [Range(1, 9999999999, ErrorMessage = "Invalid Customer ID")]
        public long? id { get; set; }

        [EmailAddress(ErrorMessage = "Invalid Email")]
        public string email { get; set; }

        public bool IsEmpty()
        {
            return !id.HasValue && string.IsNullOrEmpty(email);
        }
    }
}
