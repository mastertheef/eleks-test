﻿using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using EleksTest.API.Models;
using EleksTest.DTO;
using EleksTest.Services;
using Microsoft.AspNetCore.Mvc;

namespace EleksTest.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CustomersController : ControllerBase
    {
        private readonly ICustomerService _customerService;
        private readonly IMapper _mapper;
        private const long maxIdValue = 9999999999;

        public CustomersController(ICustomerService customerService, IMapper mapper)
        {
            _customerService = customerService;
            _mapper = mapper;
        }

        [HttpGet]
        public async Task<ActionResult<CustomerDto>> Get([FromQuery] GetCustomerModel model)
        {
            model.email = (model.email != null)
                ? model.email.Trim().ToLowerInvariant()
                : null;

            if (model.IsEmpty())
            {
                return BadRequest("No inquiry criteria");
            }

            if (!ModelState.IsValid)
            {
                var error = ModelState.Values.SelectMany(x => x.Errors).FirstOrDefault().ErrorMessage;
                return BadRequest(error);
            }

            var found = await _customerService.GetCustomer(model.id, model.email);
            if (found == null)
            {
                return NotFound();
            }
            var mapped = _mapper.Map<CustomerDto>(found);
            return mapped;
        }

        private bool IsValidEmail(string email)
        {
            try
            {
                var addr = new System.Net.Mail.MailAddress(email);
                return addr.Address == email;
            }
            catch
            {
                return false;
            }
        }
    }
}
