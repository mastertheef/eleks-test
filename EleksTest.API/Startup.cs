﻿using EleksTest.API.Exceptions;
using EleksTest.DAL;
using EleksTest.DAL.Repositories;
using EleksTest.ObjectModel;
using EleksTest.Services;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using EleksTest.API.AutoMapper;
using EleksTest.API.DataBase;
using Microsoft.Extensions.Options;

namespace EleksTest.API
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.Configure<AppConfig>(Configuration.GetSection("AppConfig"));
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);
            services.AddDbContext<EleksDbContext>(options => options.UseLazyLoadingProxies().UseSqlServer(Configuration.GetConnectionString("DefaultConnection")));
            services.AddAutomapper();
            services.AddScoped<ICustomerRepository, CustomerRepository>();
            services.AddScoped<IRepositoryBase<Transaction>, RepositoryBase<Transaction>>();
            services.AddScoped<ICustomerService, CustomerService>();
            services.AddScoped<IServiceBase<Transaction>, ServiceBase<Transaction>>();
            
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.ConfigureExceptionHandler();
            app.UseMvc();
            app.CreateDataBaseAndSeed();
        }
    }
}
